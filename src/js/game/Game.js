import Config from './Config';
import Engine from './Engine';
import PlayerManager from './PlayerManager';

export default class Game {

    constructor(canvasID, settings, onCollision, onRoundOver) {
        this._config = Object.assign({}, Config, settings);
        this._canvasElement = document.getElementById(canvasID);

        if (this._config.useFullscreen) {
            this._config.canvasWidth = window.innerWidth;
            this._config.canvasHeight = window.innerHeight;
        }

        this._canvasElement.width = this._config.canvasWidth;
        this._canvasElement.height = this._config.canvasHeight;

        if (this._canvasElement.getContext) {
            this._drawingContext = this._canvasElement.getContext('2d');
        } else {
            throw 'No canvas support';
        }

        this._playerManager = new PlayerManager(this._config);
        this._engine = new Engine(this._drawingContext, this._playerManager, this._config, onCollision, onRoundOver);
    }

    updateConfig(settings) {
        Object.keys(settings).forEach(key => this._config[key] = settings[key]);
        this._canvasElement.width = this._config.canvasWidth;
        this._canvasElement.height = this._config.canvasHeight;
    }

    start(onRedraw) {
        this._drawFrame();

        if (this._playerManager.getNumberOfPlayers() < 2) {
            return;
        }

        this._playerManager.initializePlayers();
        this._engine.start(onRedraw);
    }

    restart() {
        this._engine.stop();
        this._drawingContext.clearRect(0, 0, this._config.canvasWidth, this._config.canvasHeight);
        this.start();
    }

    stop() {
        this._engine.stop();
    }

    addPlayer(name) {
        return this._playerManager.addPlayer(name);
    }

    removePlayer(player) {
        this._playerManager.removePlayer(player);
        if (this._playerManager.getNumberOfActivePlayers() < 2) {
            this.stop();
        }
    }

    refreshColor(player) {
        this._playerManager.refreshColor(player);
    }

    drawAll(drawDatas) {
        this._engine.drawAll(drawDatas);
    }

    _drawFrame() {
        if (!this._config._drawFrame) {
            return;
        }
        this._drawingContext.lineWidth = 10;
        this._drawingContext.strokeStyle = '#E3D42E';
        this._drawingContext.strokeRect(0, 0, this._config.canvasWidth, this._config.canvasHeight);
    }

}
