export default class Player {

    constructor(id, name, color, config) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.config = config;

        this.x = 200;
        this.y = 200;
        this.speed = 1;
        this.direction = 0; // full-left -1, full-right 1
        this.angle = 0;
        this.hole = 0;

        this.isPlaying = false;
        this.isAlive = false;
        this.isRemoved = false;

        this._holeTimeoutID = 0;
    }

    navigate(direction) {
        this.direction = Math.min(Math.max(direction, -1), 1);
    }

    resetHoleTimeout() {
        clearTimeout(this._holeTimeoutID);
        this.calculateNextHole();
    }

    calculateNextHole() {
        const time = this.config.holeIntervalMin + Math.random() * (this.config.holeIntervalMax - this.config.holeIntervalMin);

        this._holeTimeoutID = setTimeout(() => {
            this.hole = this.config.holeSize;
        }, time * 1000);
    }

}
