import {requestAnimationFrame} from './utils';

export default class Engine {

    constructor(drawingContext, playerManager, config, onCollision, onRoundOver) {
        this._drawingContext = drawingContext;
        this._playerManager = playerManager;
        this._config = config;

        this._onCollision = onCollision;
        this._onRoundOver = onRoundOver;
        this._onUpdate = null;

        this._running = false;
        this._lastUpdate = Date.now();
    }

    start(onUpdate) {
        if (this._running) {
            return;
        }
        this._running = true;

        if (onUpdate) {
            this._onUpdate = onUpdate;
        }

        this._lastUpdate = Date.now();
        requestAnimationFrame(() => this._update());
    }

    stop() {
        this._running = false;
    }

    isRunning() {
        return this._running;
    }

    drawAll(drawDatas) {
        drawDatas.forEach(drawData => this._drawPlayer(drawData));
    }

    _update() {
        if (!this._running) {
            return;
        }
        const now = Date.now();
        const deltaTime = now - this._lastUpdate;
        if (deltaTime < 1000 / this._config.maxFrames) {
            requestAnimationFrame(() => this._update());
            return;
        }
        this._lastUpdate = now;
        this._calculate(deltaTime);
        requestAnimationFrame(() => this._update());
    }

    _calculate(deltaTime) {
        let player,
            deltaX,
            deltaY,
            hit = false;

        const players = this._playerManager.getActivePlayers();
        const drawDatas = [];
        for (let i = 0; i < players.length; ++i) {
            player = players[i];

            const speed = this._config.pixelsPerSecond * deltaTime / 1000;
            player.angle += player.direction * this._config.degreesPerSecond * deltaTime / 1000;
            player.angle %= 360;

            deltaX = Math.cos(player.angle * Math.PI / 180) * speed;
            deltaY = Math.sin(player.angle * Math.PI / 180) * speed;

            if (player.hole <= 0) {
                if (this._hitTest({x: player.x + deltaX, y: player.y + deltaY})) {
                    player.isAlive = false;
                    hit = true;
                    if (this._onCollision) {
                        this._onCollision(player);
                    }

                    if (players.filter(player => player.isAlive).length < 2) {
                        this.stop();
                        if (this._onRoundOver) {
                            this._onRoundOver();
                        }
                        return;
                    }
                }
                const drawData = {
                    color: player.color,
                    x: player.x,
                    y: player.y,
                    deltaX,
                    deltaY
                };
                this._drawPlayer(drawData);
                drawDatas.push(drawData);
            } else {
                player.hole -= speed;

                if (player.hole <= 0) {
                    player.calculateNextHole();
                }
            }

            player.x += deltaX;
            player.y += deltaY;
        }

        if (this._onUpdate) {
            this._onUpdate(drawDatas);
        }
    }

    _drawPlayer(drawData) {
        this._drawingContext.strokeStyle = drawData.color;
        this._drawingContext.fillStyle = drawData.color;
        this._drawingContext.beginPath();
        this._drawingContext.lineWidth = this._config.lineWidth;
        this._drawingContext.moveTo(drawData.x, drawData.y);
        this._drawingContext.lineTo(drawData.x + drawData.deltaX, drawData.y + drawData.deltaY);
        this._drawingContext.stroke();
    }

    _hitTest(point) {
        // TODO : update collision check -> do not read canvas but store allowed positions + interpolate (no jumping over lines)
        if (point.x > this._config.canvasWidth || point.y > this._config.canvasHeight || point.x < 0 || point.y < 0) {
            return true;
        }

        if (this._drawingContext.getImageData(point.x, point.y, 1, 1).data[3] > this._config.threshold) {
            return true;
        }

        return false;
    }

}
