import ColorManager from './ColorManager';
import Player from './Player';
import {random} from './utils';

export default class PlayerManager {

    constructor(config) {
        this._config = config;
        this._playerCount = 0;
        this._players = [];
        this._playersById = {};
        this._colorManager = new ColorManager(config);
    }

    addPlayer(name) {
        const id = ++this._playerCount;
        const player = new Player(id, name, this._colorManager.getColorHex(), this._config);
        this._players.push(player);
        this._playersById[id] = player;
        return player;
    }

    removePlayer(player) {
        player.isRemoved = true;
        delete this._playersById[player.id];
        this._players = this._players.filter(currPlayer => currPlayer !== player);
    }

    refreshColor(player) {
        player.color = this._colorManager.getColorHex();
    }

    initializePlayers() {
        for (let i = 0; i < this._players.length; i++) {
            const player = this._players[i];
            player.x = random(this._config.canvasWidth / 4, 3 * this._config.canvasWidth / 4);
            player.y = random(this._config.canvasHeight / 4, 3 * this._config.canvasHeight / 4);
            player.angle = Math.random() * 360;
            player.isPlaying = true;
            player.isAlive = true;
            player.resetHoleTimeout();
        }
    }

    getNumberOfActivePlayers() {
        return this.getActivePlayers().length;
    }

    getNumberOfPlayers() {
        return this._players.length;
    }

    getPlayerByID(playerID) {
        return this._playersById[playerID];
    }

    getActivePlayers() {
        return this._players.filter(player => player.isPlaying && player.isAlive);
    }

}
