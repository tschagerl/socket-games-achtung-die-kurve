export default {
    // game settings
    maxFrames: 100,

    // board settings
    canvasWidth: 0,
    canvasHeight: 0,
    drawFrame: true,        // draw border around game-board
    useFullscreen: false,

    // line settings
    lineWidth: 3,
    pixelsPerSecond: 100,   // pixels per second
    degreesPerSecond: 180,  // rotation speed of lines
    holeIntervalMin: 5,     // new hole will occur at max all x seconds
    holeIntervalMax: 10,    // new hole will occur at min all x seconds
    holeSize: 10,           // the size of the holes in pixels

    // color settings
    threshold: 100,         // alpha channel threshold for hit-check
    colorSaturation: 0.99,
    colorValue: 0.99
};
