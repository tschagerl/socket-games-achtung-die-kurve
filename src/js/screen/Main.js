import $ from 'jquery';
import qrcodejs from 'qrcodejs';

import Game from '../game/Game';

export default class Main {

    constructor(api) {
        this._api = api;

        // DOM
        this._$game = $('.content');
        this._$scores = $('.scores');

        // GAME
        this._game = null;
        this._playersByControllerId = {};
        this._rankedPlayers = [];
        this._playerCounter = 0;
        this._spectators = [];
    }

    run() {
        Main._renderScreenId();

        // init Achtung Die Kurve
        this._game = new Game('screenCanvas-adk', {
            canvasWidth: this._$game.outerWidth(),
            canvasHeight: this._$game.outerHeight(),
            drawFrame: false
        }, () => this._onCollisionCallback(), () => this._onRoundCallback());
        $(window).on('resize', () => this._onWindowResize());

        this._api.on('adk-player-join', (data, controllerId, callback) => this._onPlayerJoin(data, controllerId, callback));
        this._api.on('spectator-join', (data, controllerId, callback) => {
            this._spectators.push(controllerId);
            callback(this._game._config);
        });

        this._api.on('controller-disconnected', controllerId => this._onPlayerDisconnect(controllerId));

        this._api.on('adk-exit', () => {
            setTimeout(() => {
                this._api.emit('screen-adk-exit');
                // TODO : use socket-games-api
                top.JSCONST.stopGame();
            }, 10);
        });

        this._api.on('adk-move', (data, controllerId) => {
            const player = this._playersByControllerId[controllerId];
            player.navigate(data.direction);
        });

        this._api.on('adk-refresh-color', (data, controllerId, callback) => {
            const player = this._playersByControllerId[controllerId];
            this._game.refreshColor(player);
            callback({
                color: player.color
            });
            $('li[data-player-id=' + player.id + '] > span.player-color').css('backgroundColor', player.color);
        });
    }

    /*
     * API CALLBACKS
     */

    _onPlayerJoin(data, controllerId, callback) {
        const player = this._game.addPlayer(data.username);
        player.score = 0;
        ++this._playerCounter;
        this._playersByControllerId[controllerId] = player;
        this._rankedPlayers.push(player);
        callback({
            color: player.color
        });
        let $li = $(`
                <li data-player-id="${player.id}">
                    <span class="player-color" style="background-color: ${player.color}"></span> 
                    <span class="score">${player.name} : ${player.score}</span>
                </li>
            `);
        this._$scores.find('ul').append($li);

        if (this._playerCounter === 2) {
            $('.content .waiting').hide();
            this._game.start(drawDatas => {
                this._spectators.forEach(controllerId => this._api.emit('screen-game-draw', drawDatas, controllerId));
            });
            this._spectators.forEach(controllerId => this._api.emit('screen-game-restart', null, controllerId));
        }
    }

    _onPlayerDisconnect(controllerId) {
        const spectatorIndex = this._spectators.indexOf(controllerId);
        if (spectatorIndex) {
            this._spectators.splice(spectatorIndex, 1);
            return;
        }

        const player = this._playersByControllerId[controllerId],
            rankingIndex = this._rankedPlayers.indexOf(player);

        this._game.removePlayer(player);
        if (rankingIndex > -1) {
            this._rankedPlayers.splice(rankingIndex, 1);
        }
        delete this._playersByControllerId[controllerId];
        $('li[data-player-id="' + player.id + '"]').remove();

        if (--this._playerCounter < 2) {
            $('.content .waiting').show();
            this._game.stop();
            this._spectators.forEach(controllerId => this._api.emit('screen-game-stop', null, controllerId));
        }
    }

    _updateStats() {
        this._rankedPlayers.sort((playerA, playerB) => {
            if (playerA.score > playerB.score) {
                return -1;
            } else if (playerA.score < playerB.score) {
                return 1;
            } else {
                return 0;
            }
        });
        this._rankedPlayers.forEach(player => {
            let $li = $('li[data-player-id="' + player.id + '"]');
            if (!player.isPlaying || !player.isAlive) {
                $li.addClass('is-dead');
            }
            $li.find('.score').html(player.name + ' : ' + player.score);
            $li.detach().appendTo(this._$scores.find('ul'));
        });
        this._spectators.forEach(controllerId => this._api.emit('screen-game-stats', this._rankedPlayers, controllerId));
    }

    _onRoundCallback() {
        setTimeout(() => {
            $('li.is-dead').removeClass('is-dead');
            this._game.restart();
            this._updateStats();
            this._spectators.forEach(controllerId => this._api.emit('screen-game-restart', null, controllerId));
        }, 2000);
    }

    _onCollisionCallback() {
        this._rankedPlayers.forEach(player => {
            if (player.isPlaying && player.isAlive) {
                ++player.score;
            }
        });
        this._updateStats();
    }

    _onWindowResize() {
        this._game.updateConfig({
            canvasWidth: this._$game.outerWidth(),
            canvasHeight: this._$game.outerHeight()
        });
        this._spectators.forEach(controllerId => this._api.emit('screen-game-specs', this._game._config, controllerId));
    }

    static _renderScreenId() {
        // TODO : show big image that explains how to connect smartphone
        // ATTENTION: this method has a duplicate in gamecenter MainView
        const $screenId = $('.controller-link');
        const url = top.JSCONST.gameCenterControllerUrl;
        $screenId.html('<a href="' + url + '" target="_blank">' + url + '</a>');
        new qrcodejs.QRCode($('.qrcode')[0], url);
    }

};
