import {GameScreen} from 'socket-games-api/screen';

import Main from './Main';

const api = new GameScreen(
    () => {
        // TODO : use socket-games-api
        top.JSCONST.onGameCreated();
        const main = new Main(api);
        main.run();
    },
    error => {
        console.error('screen/index - unable to create api', error);
    }
);
