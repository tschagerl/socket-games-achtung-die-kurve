import $ from 'jquery';

export default class Main {
    constructor(api) {
        this._api = api;
        this._color = null;
        this._currentDirection = 0;

        // TODO : use socket-games-api
        this._username = localStorage.getItem('username');
        if (typeof this._username !== 'string') {
            this._username = 'Guest';
        }
    }

    run() {
        this._api.emit('adk-player-join', {
            username: this._username
        }, data => {
            this._color = data.color;
            $('body').css({
                borderColor: this._color
            });
            $('.exit').css({
                background: this._color
            });
            $('.refresh-color').css({
                background: this._color
            });
            this._emitDirection();

            navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
            if (navigator.vibrate) {
                navigator.vibrate(100);
            }
        });

        $(top.document).on('keydown', e => {
            switch (e.keyCode) {
                case 37:
                    e.preventDefault();
                    this._left();
                    break;
                case 39:
                    e.preventDefault();
                    this._right();
                    break;
                default:
                    break;
            }
        });

        $(top.document).on('keyup', e => {
            switch (e.keyCode) {
                case 37:
                    e.preventDefault();
                    this._releaseLeft();
                    break;
                case 39:
                    e.preventDefault();
                    this._releaseRight();
                    break;
                default:
                    break;
            }
        });

        $('.move-left').on('mousedown touchstart', e => {
            e.preventDefault();
            e.stopPropagation();
            this._left();
        });

        $('.move-right').on('mousedown touchstart', e => {
            e.preventDefault();
            e.stopPropagation();
            this._right();
        });

        $('.move-left').on('mouseup touchend', e => {
            e.preventDefault();
            e.stopPropagation();
            this._releaseLeft();
        });

        $('.move-right').on('mouseup touchend', e => {
            e.preventDefault();
            e.stopPropagation();
            this._releaseRight();
        });

        $('.exit').on('mousedown touchstart', function (e) {
            $(this).addClass('pressed');
        });
        $('.exit').on('mouseup touchend', function (e) {
            $(this).removeClass('pressed');
        });
        $('.exit').on('click', e => {
            e.preventDefault();
            if (confirm('really?')) {
                this._api.emit('adk-exit');
            }
        });
        $('.refresh-color').on('mousedown touchstart', function (e) {
            $(this).addClass('pressed');
        });
        $('.refresh-color').on('mouseup touchend', function (e) {
            $(this).removeClass('pressed');
        });
        $('.refresh-color').on('click', e => {
            e.preventDefault();
            this._api.emit('adk-refresh-color', {}, data => {
                this._color = data.color;
                $('body').css({
                    borderColor: this._color
                });
                $('.exit').css({
                    background: this._color
                });
                $('.refresh-color').css({
                    background: this._color
                });
            });
        });

        // TODO : use socket-games-api
        this._api.on('screen-adk-exit', () => {
            top.JSCONST.stopGame();
        });
        this._api.on('screen-disconnected', () => {
            top.JSCONST.stopGame();
        });
    }


    _left() {
        $('.move-left').addClass('pressed');
        this._currentDirection = -1;
        this._emitDirection();
    }

    _right() {
        $('.move-right').addClass('pressed');
        this._currentDirection = 1;
        this._emitDirection();
    }

    _releaseLeft() {
        $('.move-left').removeClass('pressed');
        if (this._currentDirection !== -1) {
            return;
        }
        this._currentDirection = 0;
        this._emitDirection();
    }

    _releaseRight() {
        $('.move-right').removeClass('pressed');
        if (this._currentDirection !== 1) {
            return;
        }
        this._currentDirection = 0;
        this._emitDirection();
    }

    _emitDirection() {
        this._api.emit('adk-move', {
            direction: this._currentDirection
        });
    }
}
