import $ from 'jquery';
import qrcodejs from 'qrcodejs';

import Game from '../game/Game';

export default class Main {

    constructor(api) {
        this._api = api;

        // DOM
        this._$scores = $('.scores ul');

        // GAME
        this._game = null;
    }

    run() {
        Main._renderScreenId();

        this._api.emit('spectator-join', {}, data => this._setupGame(data));

        // TODO : use socket-games-api
        this._api.on('screen-adk-exit', () => {
            top.JSCONST.stopGame();
        });
        this._api.on('screen-disconnected', () => {
            top.JSCONST.stopGame();
        });
    }

    /*
     * API CALLBACKS
     */

    _setupGame(data) {
        // init Achtung Die Kurve
        this._game = new Game('game', {
            canvasWidth: data.canvasWidth,
            canvasHeight: data.canvasHeight,
            lineWidth: data.lineWidth,
            drawFrame: data.drawFrame
        });
        $(window).on('resize', () => this._onWindowResize());
        this._onWindowResize();

        this._api.on('screen-game-specs', data => this._updateGameSpecs(data));
        this._api.on('screen-game-draw', drawData => this._game.drawAll(drawData));
        this._api.on('screen-game-stats', stats => this._updateStats(stats));
        this._api.on('screen-game-restart', () => {
            $('.waiting').hide();
            this._game._drawingContext.clearRect(0, 0, this._game._config.canvasWidth, this._game._config.canvasHeight);
        });
        this._api.on('screen-game-stop', () => $('.waiting').show());
    }

    _updateGameSpecs(data) {
        this._game.updateConfig({
            canvasWidth: data.canvasWidth,
            canvasHeight: data.canvasHeight
        });
        this._onWindowResize();
    }

    _updateStats(stats) {
        this._$scores.html('');
        stats.forEach(player => {
            const $li = $(`
                <li>
                    <span class="player-color" style="background-color: ${player.color}"></span> 
                    <span class="score">${player.name} : ${player.score}</span>
                </li>
            `);
            if (!player.isPlaying || !player.isAlive) {
                $li.addClass('is-dead');
            }
            this._$scores.append($li);
        });
    }

    _onWindowResize() {
        let scale = $('.content').outerWidth() / this._game._config.canvasWidth;
        scale = Math.min(scale, $('.content').outerHeight() / this._game._config.canvasHeight);
        $('#game').css('transform', 'scale(' + scale + ')');
    }

    static _renderScreenId() {
        // TODO : show big image that explains how to connect smartphone
        // ATTENTION: this method has a duplicate in gamecenter MainView
        const $screenId = $('.controller-link');
        const url = top.JSCONST.gameCenterControllerUrl;
        $screenId.html('<a href="' + url + '" target="_blank">' + url + '</a>');
        new qrcodejs.QRCode($('.qrcode')[0], url);
    }

};
