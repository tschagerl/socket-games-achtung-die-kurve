import {GameController} from 'socket-games-api/controller';

import Main from './Main';

const api = new GameController(
    () => {
        const main = new Main(api);
        main.run();
    },
    error => {
        console.error('spectator/index - unable to create api', error);
    }
);
